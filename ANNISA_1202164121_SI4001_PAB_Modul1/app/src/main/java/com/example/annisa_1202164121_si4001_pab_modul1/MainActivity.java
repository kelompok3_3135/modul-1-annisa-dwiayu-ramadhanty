package com.example.annisa_1202164121_si4001_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText lahan_alas, lahan_tinggi;
    private Button btn_cek;
    private TextView hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lahan_alas = (EditText) findViewById(R.id.alas);
        lahan_tinggi = (EditText) findViewById(R.id.tinggi);
        btn_cek = (Button) findViewById(R.id.cek_hasil);
        hasil = (TextView) findViewById(R.id.hasil);

    }

    public void hitung(View view) {
        int angka_alas = Integer.parseInt(lahan_alas.getText().toString());
        int angka_tinggi = Integer.parseInt(lahan_tinggi.getText().toString());
        int angka_hasil = angka_alas*angka_tinggi;
        hasil.setText(String.valueOf(angka_hasil));

    }
}
